#-----------------------------------------
# Variables
#-----------------------------------------
MKFILE_PATH := $(abspath $(lastword ${MAKEFILE_LIST}))
PROJECT_PATH := $(dir ${MKFILE_PATH})
PROJECT_NAME := $(shell basename ${PROJECT_PATH})
UID=$(shell id -u)
export UID
GID=$(shell id -g)
export GID

#-----------------------------------------
# Help commands
#-----------------------------------------
.PHONY:
.DEFAULT_GOAL := help

help: ## Prints this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' ${MAKEFILE_LIST} | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

run: build ## Build and run Kubetools
	docker run \
		-it --rm \
		--network=host \
		--volume "${HOME}/.kube/config:/root/.kube/config:ro" \
		--volume "${HOME}/.minikube:${HOME}/.minikube:ro" \
		--volume "${PROJECT_PATH}:/project" \
		registry.gitlab.com/savadenn-public/kubetools

build: ## Build Kubetools
	docker build -t registry.gitlab.com/savadenn-public/kubetools .