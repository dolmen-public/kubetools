FROM registry.gitlab.com/savadenn-public/runners/kube:latest

RUN apk update && \
    apk add --no-cache \
    ncurses \
    zsh \
    && \
    rm -rf /tmp/* /etc/apk/cache/*

# Add comfort
# https://agrimprasad.com/post/supercharge-kubernetes-setup/
#   stern   https://github.com/wercker/stern
#   popeye  https://github.com/derailed/popeye
#   lubectx https://github.com/ahmetb/kubectx
#   jq      https://github.com/stedolan/jq
#   yq      https://github.com/mikefarah/yq
#   sealed  https://github.com/bitnami-labs/sealed-secrets
ENV STERN_VERSION=1.11.0 \
    POPEYE_VERSION=0.10.0 \
    KUBECTX_VERSION=0.9.4 \
    JQ_VERSION=jq-1.6 \
    YQ_VERSION=3.4.0 \
    SEALED_VERSION=0.17.5 \
    ZSH="/root/.oh-my-zsh"

RUN curl -fsSL -o /usr/local/bin/stern https://github.com/wercker/stern/releases/download/${STERN_VERSION}/stern_linux_amd64 \
    && chmod 700 /usr/local/bin/stern \
    && curl -fsSL https://github.com/derailed/popeye/releases/download/v${POPEYE_VERSION}/popeye_Linux_x86_64.tar.gz | tar -xz \
    && chmod +x ./popeye \
    && mv ./popeye /usr/local/bin/popeye \
    && curl -fsSL -o /usr/local/bin/kubectx https://github.com/ahmetb/kubectx/releases/download/v${KUBECTX_VERSION}/kubectx \
    && chmod 700 /usr/local/bin/kubectx \
    && curl -fsSL -o /usr/local/bin/kubens https://github.com/ahmetb/kubectx/releases/download/v${KUBECTX_VERSION}/kubens \
    && chmod 700 /usr/local/bin/kubens \
    && git clone https://github.com/ohmyzsh/ohmyzsh.git ${ZSH} \
    && curl -fsSL -o /usr/local/bin/kube-ps1.sh "https://raw.githubusercontent.com/jonmosco/kube-ps1/master/kube-ps1.sh" \
    && chmod 700 /usr/local/bin/kube-ps1.sh \
    && wget https://github.com/stedolan/jq/releases/download/${JQ_VERSION}/jq-linux64 -O /usr/bin/jq \
    && chmod +x /usr/bin/jq \
    && wget https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/yq_linux_amd64 -O /usr/bin/yq \
    && chmod +x /usr/bin/yq \
    && curl -fsSL https://github.com/bitnami-labs/sealed-secrets/releases/download/v${SEALED_VERSION}/kubeseal-${SEALED_VERSION}-linux-amd64.tar.gz | tar -xz \
    && chmod +x ./kubeseal \
    && mv ./kubeseal /usr/local/bin/kubeseal

COPY .zshrc  /root/.zshrc

WORKDIR /project

ENTRYPOINT [ "/bin/zsh" ]